package FinalProjectT.com.example.FinaProjectT.controller;

import FinalProjectT.com.example.FinaProjectT.exception.UserNotFoundException;
import FinalProjectT.com.example.FinaProjectT.model.User;
import FinalProjectT.com.example.FinaProjectT.repository.UserRepository;
import FinalProjectT.com.example.FinaProjectT.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {


    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;

    @Test
    public void testAddUser() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        User user = new User();
        user.setEmail("Soting@ting.com");
        user.setPhoneNumber("23142423");

        when(userService.save(any(User.class))).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.save(user);

        assertEquals(409, responseEntity.getStatusCodeValue());
    }
    @Autowired
    UserRepository userRepository;


    @Test
    public void testDeleteUser() throws UserNotFoundException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        User user = new User();
        user.setUserName("Pedro");
        user.setPhoneNumber("345434");

        when(userService.delete("Pedro")).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.delete(user.getUserName());

        assertEquals(409, responseEntity.getStatusCodeValue());
    }






}


