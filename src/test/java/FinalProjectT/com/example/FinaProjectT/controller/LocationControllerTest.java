package FinalProjectT.com.example.FinaProjectT.controller;

import FinalProjectT.com.example.FinaProjectT.model.Location;
import FinalProjectT.com.example.FinaProjectT.service.LocationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;


@RunWith(MockitoJUnitRunner.class)
public class LocationControllerTest {
    @InjectMocks
    LocationController locationController;
    @Mock
    LocationService locationService;

    @Test
    public void testAddedLocation(){
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Location location = new Location();

        location.setAltitude(321);
        location.setLongitude(321);
        location.setLatitude(321);

        Mockito.lenient().when(locationService.save(any(Location.class))).thenReturn(location);

        ResponseEntity<Location> responseEntity = locationController.save(location);

        assertEquals(409, responseEntity.getStatusCodeValue());
    }
}
