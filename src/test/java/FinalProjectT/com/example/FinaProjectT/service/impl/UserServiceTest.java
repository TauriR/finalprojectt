package FinalProjectT.com.example.FinaProjectT.service.impl;

import FinalProjectT.com.example.FinaProjectT.model.User;
import FinalProjectT.com.example.FinaProjectT.repository.UserRepository;
import FinalProjectT.com.example.FinaProjectT.service.RoleService;
import FinalProjectT.com.example.FinaProjectT.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    private User MyFirstUser;

    @Autowired
    UserRepository userRepository;

    @Before
    public void setup(){
        MyFirstUser = new User();
        MyFirstUser.setId(1l);
        MyFirstUser.setUserName("Mastah");
        MyFirstUser.setName("Peter");
        MyFirstUser.setLastName("Padro");
        MyFirstUser.setWeight("200");
        MyFirstUser.setPhoneNumber("3424322532");
        MyFirstUser.setHeight("100");
        MyFirstUser.setEmail("Dingo@mail.com");
        MyFirstUser.setBirthDate("12/12/2000");
        MyFirstUser.setActive(true);

        User mahuser = userRepository.save(MyFirstUser);
        System.out.println(mahuser.getId());

    }
    @Test
    public void findByIdTestReturnSuccess() throws Exception{
        //given
        UserServiceImpl userService = new UserServiceImpl(userRepository, roleService);
        //when
        User user = userService.findById(1l);//(anyLong()
        //when
        Assert.assertNotNull(user.getId());
    }

    @Test
    public void findByUserNameTestReturnSuccess() throws Exception{
        UserServiceImpl userService = new UserServiceImpl(userRepository, roleService);
        User user = userService.findByUserName("Mastah");
        Assert.assertNotNull(user.getUserName());
    }
//    @Test
//    public void deleteUserbyUserId_ShouldReturnStatusOk() throws Exception{
//        given(userService.findById(MyUserId)).willReturn(MyFirstUser);
//        Mockito.doNothing().when(userService).delete(any(String.class));
//
//        mvc.perform(delete("user/delete/{id}",
//        MyUserId)
//                .contentType(PageAttributes.MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk)
//
//    }

    @Test
    public  void deleteUserByUserNameTestSuccess(){
        UserServiceImpl userService = new UserServiceImpl(userRepository, roleService);
        User user = userService.delete("XxSupaHotxX");
//        Assert.assertNotNull(delete("/user/delete/{id}"));
    }


}
