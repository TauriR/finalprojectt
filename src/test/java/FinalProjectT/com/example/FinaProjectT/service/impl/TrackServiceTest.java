package FinalProjectT.com.example.FinaProjectT.service.impl;

import FinalProjectT.com.example.FinaProjectT.model.Track;
import FinalProjectT.com.example.FinaProjectT.repository.TrackRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TrackServiceTest {
    @Autowired
    TrackServiceImpl trackService;

    @Autowired
    TrackRepository trackRepository;

    @Test
    public void saveTrackByIdReturnSuccess() throws Exception{
        Track track = new Track();
        track.setStart("String address Start");
        track.setEnd("String address end");

        TrackServiceImpl trackService = new TrackServiceImpl(trackRepository);
        Track track1 = trackService.save(track);
        Assert.assertNotNull(track1.getId());
    }
}
