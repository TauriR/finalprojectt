package FinalProjectT.com.example.FinaProjectT.service;


import FinalProjectT.com.example.FinaProjectT.model.Location;

import java.util.Optional;


public interface LocationService {

    Location save(Location location);

    Optional<Location> findById(Long id);
}



