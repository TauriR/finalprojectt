package FinalProjectT.com.example.FinaProjectT.service.impl;

import FinalProjectT.com.example.FinaProjectT.exception.EmailAlreadyExistsException;
import FinalProjectT.com.example.FinaProjectT.exception.UserNameAlreadyExistsException;
import FinalProjectT.com.example.FinaProjectT.model.Role;
import FinalProjectT.com.example.FinaProjectT.model.User;
import FinalProjectT.com.example.FinaProjectT.repository.UserRepository;
import FinalProjectT.com.example.FinaProjectT.service.RoleService;
import FinalProjectT.com.example.FinaProjectT.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleService roleService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleService roleService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
    }

    public User findById(Long id) {
        return userRepository.findById(id).get();
    }


    @Override
    public User save(User user) {

        return userRepository.save(user);
    }

    @Override
    public User findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }

    @Override
    public User delete(String userName) {
        return userRepository.findByUserName(userName);
    }

    @Override
    public User updateWeight(String weight) {
        return null;
    }



    @Override
    public User add(User user) {
        return null;
    }

    @Override
    public User block(String userName) {
        return null;
    }


    @Override
    public List<User> getUserList() {
        return userRepository.findAll();
    }
    @Override
    public User register(User user) throws EmailAlreadyExistsException, UserNameAlreadyExistsException {
        if (userRepository.findByUsername(user.getUserName()) == null) {
            if (userRepository.findByEmail(user.getEmail()) == null) {
                Role role = roleService.getByName(user.getRole().getName());
                user.setRole(role);
                return userRepository.save(user);
            } else throw new EmailAlreadyExistsException(user.getEmail());
        } else throw new UserNameAlreadyExistsException(user.getUserName());
    }
    @Override
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "Success";
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();

    }


}

