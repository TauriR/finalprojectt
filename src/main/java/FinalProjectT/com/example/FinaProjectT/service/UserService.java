package FinalProjectT.com.example.FinaProjectT.service;

import FinalProjectT.com.example.FinaProjectT.exception.EmailAlreadyExistsException;
import FinalProjectT.com.example.FinaProjectT.exception.UserNameAlreadyExistsException;
import FinalProjectT.com.example.FinaProjectT.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface UserService {
    User findById(Long id);

    User save(User user);

    User findByUserName(String userName);

    User delete(String userName);

    User updateWeight(String weight);

    User add(User user);

    User block(String userName);

    List<User> getUserList();

    User register(User user) throws EmailAlreadyExistsException, UserNameAlreadyExistsException;

    String logout(HttpServletRequest request, HttpServletResponse response);

    List<User> findAll();
}
