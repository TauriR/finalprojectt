package FinalProjectT.com.example.FinaProjectT.service.impl;

import FinalProjectT.com.example.FinaProjectT.model.Location;
import FinalProjectT.com.example.FinaProjectT.repository.LocationRepository;
import FinalProjectT.com.example.FinaProjectT.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LocationServiceImpl implements LocationService {
    private final LocationRepository locationRepository;


    @Autowired
    public LocationServiceImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public Location save(Location location) {
        return save(location);
    }

    @Override
    public Optional<Location> findById(Long id) {
        return locationRepository.findById(id);
    }
}
