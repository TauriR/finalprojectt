package FinalProjectT.com.example.FinaProjectT.service;


import FinalProjectT.com.example.FinaProjectT.model.Role;

public interface RoleService {
    Role getRole(Long id);
    Role add(Role role);
    Role getByName(String name );
}
