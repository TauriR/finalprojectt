package FinalProjectT.com.example.FinaProjectT.service.impl;

import FinalProjectT.com.example.FinaProjectT.model.Track;
import FinalProjectT.com.example.FinaProjectT.repository.TrackRepository;
import FinalProjectT.com.example.FinaProjectT.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrackServiceImpl implements TrackService {
    private final TrackRepository trackRepository;


    @Autowired
    public TrackServiceImpl(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    @Override
    public Track save(Track track) {
        return trackRepository.save(track);
    }
}
