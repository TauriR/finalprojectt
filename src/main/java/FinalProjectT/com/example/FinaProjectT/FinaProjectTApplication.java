package FinalProjectT.com.example.FinaProjectT;


import FinalProjectT.com.example.FinaProjectT.model.Role;
import FinalProjectT.com.example.FinaProjectT.model.User;
import FinalProjectT.com.example.FinaProjectT.repository.RoleRepository;
import FinalProjectT.com.example.FinaProjectT.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.*;

@SpringBootApplication
@EnableSwagger2
public class FinaProjectTApplication implements CommandLineRunner {




	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("FinalProjectT.com.example.FinaProjectT")).build();
	}



	private final UserService userService;
	private final RoleRepository roleRepository;

	@Autowired
	public FinaProjectTApplication(UserService userService, RoleRepository roleRepository) {
		this.userService = userService;
		this.roleRepository = roleRepository;
	}
	public static void main(String[] args) {
		SpringApplication.run(FinaProjectTApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {
		Role role = new Role();
		role.setName("ADMIN");
		roleRepository.save(role);
		Role role2 = new Role();
		role2.setName("USER");
		roleRepository.save(role2);

		User firstUser = new User();

		firstUser.setActive(true);
		firstUser.setBirthDate("01.01.1993");
		firstUser.setEmail("Dingo@tango.com");
		firstUser.setHeight("188");
		firstUser.setPassword("makaronid");
		firstUser.setPhoneNumber("64352346");
		firstUser.setUserName("XxSupaHotxX");
		firstUser.setWeight("120");
		firstUser.setName("Peter");
		firstUser.setLastName("Dingo");
		firstUser.setRole(role2);

		User secUser = new User();
		secUser.setActive(true);
		secUser.setBirthDate("01.01.2000");
		secUser.setEmail("Pedro@tango.com");
		secUser.setHeight("200");
		secUser.setPassword("kiirnuudlid");
		secUser.setPhoneNumber("1232346");
		secUser.setUserName("MastahMan");
		secUser.setWeight("100");
		secUser.setName("Minah");
		secUser.setLastName("Pedro");
		secUser.setRole(role2);

		User thirdUser = new User();
		thirdUser.setActive(true);
		thirdUser.setBirthDate("01.01.2012");
		thirdUser.setEmail("Kakari@tango.com");
		thirdUser.setHeight("140");
		thirdUser.setPassword("PizzaPasta");
		thirdUser.setPhoneNumber("5532346");
		thirdUser.setUserName("Manh123");
		thirdUser.setWeight("76");
		thirdUser.setName("Whadava");
		thirdUser.setLastName("Kakari");
		thirdUser.setRole(role2);

		User admin = new User();
		admin.setActive(true);
		admin.setBirthDate("01.01.2012");
		admin.setEmail("sumjung@tango.com");
		admin.setHeight("140");
		admin.setPassword("$2a$10$irKmPx58LJ0ketpDXIEwFO7QZl.xC9FcybpLrh5RC0KA.WlIyhHDq");
		admin.setPhoneNumber("5532346");
		admin.setUserName("admin");
		admin.setWeight("76");
		admin.setName("Miki");
		admin.setLastName("Heljo");
		admin.setRole(role);
		userService.save(admin);

		User savedFirstUser = userService.save(firstUser);
		User savedSecUser =userService.save(secUser);

		Set friends = new HashSet();
		friends.add(savedFirstUser);
		friends.add(savedSecUser);

		thirdUser.setFriends(friends);
		User saveThirdUser = userService.save(thirdUser);
		System.out.println(saveThirdUser);



		}
	}

