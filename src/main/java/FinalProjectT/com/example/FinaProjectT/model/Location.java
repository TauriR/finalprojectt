package FinalProjectT.com.example.FinaProjectT.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Location{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private double altitude;
    private double latitude;
    private double longitude;



    public Long getId() {
        return id;
    }


    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Location [altitude=" + altitude + ", latitude=" + latitude + ", longitude=" + longitude + "]";
    }

    public void setId(Long randomUUID) {
        this.id = randomUUID;
    }


}

