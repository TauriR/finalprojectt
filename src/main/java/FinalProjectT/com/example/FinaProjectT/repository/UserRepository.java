package FinalProjectT.com.example.FinaProjectT.repository;

import FinalProjectT.com.example.FinaProjectT.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findById(Long id);

    User findByUserName(String userName);

    @Query("FROM User WHERE userName=:username")
    User findByUsername(@Param("username") String username);
    @Query("FROM User WHERE email=:email")
    User findByEmail(@Param("email") String email);
}


