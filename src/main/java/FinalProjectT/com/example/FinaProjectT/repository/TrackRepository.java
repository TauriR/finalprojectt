package FinalProjectT.com.example.FinaProjectT.repository;

import FinalProjectT.com.example.FinaProjectT.model.Track;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrackRepository extends JpaRepository<Track, Long> {
    Track save(Track track);
}
