package FinalProjectT.com.example.FinaProjectT.repository;

import FinalProjectT.com.example.FinaProjectT.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    Location save(Long id);


    Optional<Location> findById(Long id);
}
