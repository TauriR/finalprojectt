package FinalProjectT.com.example.FinaProjectT.controller;

import FinalProjectT.com.example.FinaProjectT.model.Location;
import FinalProjectT.com.example.FinaProjectT.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class LocationController {
    private final LocationService locationService;


    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping(value = "/location/{id}")
    public ResponseEntity<Location> findCurrentLocation(@PathVariable("id") Long id) {
        Optional<Location> location = locationService.findById(id);
        if (location != null)
            return new ResponseEntity(location, HttpStatus.OK);
        else
            return new ResponseEntity("We could not find this location", HttpStatus.NOT_FOUND);
    }


    @PostMapping(value = "/location")
    public ResponseEntity<Location> save(@RequestBody Location location) {
        if (locationService.findById(location.getId()) == null) {
            Location saveLocation = locationService.save(location);
            if (saveLocation.getId() == null) {
                return new ResponseEntity("Location does not exist", HttpStatus.CONFLICT);
            } else {
                return new ResponseEntity(saveLocation, HttpStatus.CREATED);
            }
        } else {
            return new ResponseEntity("ID already existed", HttpStatus.CONFLICT);
        }
    }

}


