package FinalProjectT.com.example.FinaProjectT.controller;

import FinalProjectT.com.example.FinaProjectT.model.Role;
import FinalProjectT.com.example.FinaProjectT.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/role")
@RestController
public class RoleController {

    final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }
    @PostMapping(name = "/add")
    public Role addRole(@RequestBody Role role) {
        return roleService.add(role);
    }
    @GetMapping(value = "/{id}")
    public Role getRole(@PathVariable Long id) {
        return roleService.getRole(id);
    }
}
