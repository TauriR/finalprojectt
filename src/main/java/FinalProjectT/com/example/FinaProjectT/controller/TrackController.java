package FinalProjectT.com.example.FinaProjectT.controller;

import FinalProjectT.com.example.FinaProjectT.model.Location;
import FinalProjectT.com.example.FinaProjectT.model.Track;
import FinalProjectT.com.example.FinaProjectT.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrackController {
    private final TrackService trackService;

    @Autowired
    public TrackController(TrackService trackService) {
        this.trackService = trackService;
    }

    @PostMapping(value = "/location/track")
    public ResponseEntity<Track> save(@RequestBody Track track){
        Track savedTrack = trackService.save(track);
        if (savedTrack.getId() == null) {
            return new ResponseEntity("track does not exist", HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity(savedTrack, HttpStatus.CREATED);
        }
    }
}

