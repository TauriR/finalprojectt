package FinalProjectT.com.example.FinaProjectT.controller;

import FinalProjectT.com.example.FinaProjectT.exception.EmailAlreadyExistsException;
import FinalProjectT.com.example.FinaProjectT.exception.UserNameAlreadyExistsException;
import FinalProjectT.com.example.FinaProjectT.exception.UserNotFoundException;
import FinalProjectT.com.example.FinaProjectT.model.User;
import FinalProjectT.com.example.FinaProjectT.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {
    private final UserService userService;
    Logger logger = (Logger) LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/user")
    public ResponseEntity<User> save(@RequestBody User user) {
        logger.info("request body " + user);
//        if (userService.findById(user.getId()) == null) {
            User savedUser = userService.save(user);
            if (savedUser.getId() == null) {
                return new ResponseEntity("User already exist", HttpStatus.CONFLICT);
            } else {
                return new ResponseEntity(savedUser, HttpStatus.CREATED);
            }
//        } else {
//            return new ResponseEntity("ID already existed", HttpStatus.CONFLICT);
//        }
    }
    @GetMapping(value = "/users")
    public ResponseEntity<User> findAll() {
        List<User> users = userService.findAll();
        if (users != null)
            return new ResponseEntity(users, HttpStatus.OK);
        else
            return new ResponseEntity("We couldnt find this user: " + users, HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/user/friend/add/{username}")
    public ResponseEntity<User> addFriend(@RequestBody User user, @PathVariable String username) {
        if (userService.findByUserName(user.getUserName()) == null) {
            User addingFriend = userService.add(user);
            if (addingFriend.getUserName() == null) {
                return new ResponseEntity("User is already added", HttpStatus.CONFLICT);
            } else {
                return new ResponseEntity(addingFriend, HttpStatus.OK);
            }
        } else {
            return new ResponseEntity("User already exists", HttpStatus.CONFLICT);

        }
    }

    @PutMapping(value = "/user/{id}/update")
    public ResponseEntity<User> updatedUser(@PathVariable("id") Long userId, @RequestBody User user) {
        if (userService.findById(userId) != null) {
            User updatedUserInfo = userService.save(user);
            if (updatedUserInfo.getId() != null) {
                return new ResponseEntity("There is not this kind of user", HttpStatus.CONFLICT);
            } else {
                return new ResponseEntity(updatedUserInfo, HttpStatus.CREATED);
            }

        } else {
            return new ResponseEntity("This id is already created", HttpStatus.CONFLICT);
        }
    }


    @PatchMapping(value = "/user/friends/delete/{userName}")
    public ResponseEntity<User> delete(@PathVariable("userName") String user) throws UserNotFoundException {
        if (userService.findByUserName(user) == null) {
            throw new UserNotFoundException("Friend with username: " + "not found");
        }
        return new ResponseEntity(userService.delete(user), HttpStatus.OK);
    }

    @GetMapping(value = "/user/friends/{userName}")
    public ResponseEntity<User> FindByUserName(@PathVariable("userName") String userName) {
        User user = userService.findByUserName(userName);
        if (user != null)
            return new ResponseEntity(user, HttpStatus.OK);
        else
            return new ResponseEntity("We couldnt find this user: " + userName, HttpStatus.NOT_FOUND);
    }

    @PatchMapping(value = "/user/{weight}")
    public ResponseEntity<User> updateWeight(@PathVariable("weight") String weight) {
        User usr = userService.updateWeight(weight);
        return new ResponseEntity(usr, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/user/friends/username/{location}")
    public ResponseEntity<User> getFriendLocation(@PathVariable("location") String location){
        User user = userService.findByUserName(location);
        if(user != null)
            return new ResponseEntity(user, HttpStatus.OK);
        else
            return new ResponseEntity("We couldnt find this location: " + location, HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/user/username/{location}")
    public ResponseEntity<User> trackUser(@PathVariable("location") String location) {
        User user = userService.findByUserName(location);
        if (user != null)
            return new ResponseEntity(user, HttpStatus.OK);
        else
            return new ResponseEntity("We couldnt find this location: " + location, HttpStatus.NOT_FOUND);
    }
    @PatchMapping(value = "/user/friend/block/{userName}")
    public ResponseEntity<User> blockFriend (@PathVariable("userName") String userName){
        User usr = userService.block(userName);
        return new ResponseEntity(usr, HttpStatus.ACCEPTED);
    }

    @GetMapping(value = "/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> roleList() {
        return userService.getUserList();
    }
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public User registerUser(@RequestBody User user) throws EmailAlreadyExistsException, UserNameAlreadyExistsException {
        return userService.register(user);
    }
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response){
        return userService.logout(request,response);
    }

    @GetMapping(value="user/{id}")
    public ResponseEntity<User> findById(@PathVariable ("id") Long id) {
        logger.info("request body " + id);
        User user = userService.findById(id);
        ResponseEntity responseEntity = new ResponseEntity(user, HttpStatus.OK);
        logger.info("id is: "+responseEntity);
        return responseEntity;
    }
}


